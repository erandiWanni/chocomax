<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductAssign extends Model
{
  protected $table = 'product_assign';

  protected $fillable = [
      'product_assign_id', 'product_id', 'assign_to', '	quantity', 'comments', 'created_by', 'updated_by'
  ];
}
