<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestedProduct extends Model
{
  protected $table = 'request_product';

  protected $fillable = [
      'request_prod_id', 'shop_id', 'product_id', 'quantity', 'feedback', 'created_by', 'updated_by'
  ];
}
