<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RetailShop extends Model
{
  protected $table = 'retail_shop';

  protected $fillable = [
      'shop_id', 'shop_name', 'owner_name', 'shop_email', 'contact_number', 'address_1', 'address_2', 'city', 'created_by', 'updated_by'
  ];
}
