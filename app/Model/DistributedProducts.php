<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DistributedProducts extends Model
{
  protected $table = 'distributed_product';

  protected $fillable = [
      'distributed_prod_id', 'shop_id', 'product_id', 'quantity', 'comments', 'created_by', 'updated_by'
  ];
}
