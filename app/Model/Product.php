<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [
      'product_id', 'product_name', 'product_code', 'unit_price', 'expire_date', 'manufactured_date', 'product_image', 'created_by', 'updated_by'
  ];
}
