<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\RetailShop;
use App\Model\RequestedProduct;
use Auth;

class RequestedProductsController extends Controller
{
  public function index()
 {
    $userId = Auth::id();

     $requestedProduct = new RequestedProduct();

     $roleId = getUserInfo();

     if ($roleId == 1) {
       $requestedProducts = $requestedProduct->get();
     } else {
       $requestedProducts = $requestedProduct->where('created_by', $userId)->get();
     }

     return view('distributors.requested_products.list', compact('requestedProducts'));
  }

  public function create()
  {
      $userId = Auth::id();

      $product = new Product();
      $products = $product->get();

      $retailShop = new RetailShop();

      $roleId = getUserInfo();

      if ($roleId == 1) {
        $retailShops = $retailShop->get();
      } else {
        $retailShops = $retailShop->where('created_by', $userId)->get();
      }

      return view('distributors.requested_products.create', compact('products', 'retailShops'));
  }

  public function store(Request $request)
   {
          $userId = Auth::id();

          $requestedProduct = new RequestedProduct();
          $requestedProduct->shop_id = $request['shop_id'];
          $requestedProduct->product_id = $request['product_id'];
          $requestedProduct->quantity = $request['quantity'];
          $requestedProduct->feedback = $request['feedback'];
          $requestedProduct->created_by = $userId;
          $requestedProduct->updated_by = $userId;

          if(empty($request['requested_prod_id'])) {
            $requestedProduct->save();
          } else {
            $requestedProductInfo = $requestedProduct->where('request_prod_id', $request['requested_prod_id'])->update(
              [
                'shop_id' => $requestedProduct->shop_id,
                'product_id' => $requestedProduct->product_id,
                'quantity' => $requestedProduct->quantity,
                'feedback' => $requestedProduct->feedback,
                'updated_by' => $userId
              ]);
          }

       return redirect('/requested_products/list');
   }

   public function edit($id){

     $requestProduct = new RequestedProduct();
     $requestProductInfo = $requestProduct->where('request_prod_id', $id)->first();

     $userId = Auth::id();

     $product = new Product();
     $products = $product->get();

     $retailShop = new RetailShop();

     $roleId = getUserInfo();

     if ($roleId == 1) {
       $retailShops = $retailShop->get();
     } else {
       $retailShops = $retailShop->where('created_by', $userId)->get();
     }

     return view('distributors.requested_products.create', compact('products', 'retailShops', 'requestProductInfo'));
   }
}
