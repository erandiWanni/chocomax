<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use Auth;

class ProductController extends Controller
{
    public function index()
   {
      $product = new Product();
      $products = $product->get();

      // dd($products);
       return view('admin.products.list', compact('products'));
   }

   public function create()
  {
      return view('admin.products.create');
  }

  public function store(Request $request)
 {
        $userId = Auth::id();

        $expireDate = date('Y-m-d', strtotime(str_replace('/', '-', $request['product_expire_date'])));
        $manufactureDate = date('Y-m-d', strtotime(str_replace('/', '-', $request['product_manufacture_date'])));

        $product = new Product();
        $product->product_name = $request['product_name'];
        $product->product_code = $request['product_code'];
        $product->unit_price = $request['product_unit_price'];
        $product->expire_date = $expireDate;
        $product->manufactured_date = $manufactureDate;
        $product->created_by = $userId;
        $product->updated_by = $userId;

        $this->validate($request, [
       'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

       if ($request->hasFile('product_image')) {
           $image = $request->file('product_image');
           $name = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = public_path('/images');
           $image->move($destinationPath, $name);

           $product->product_image = $name;
       }

        if(empty($request['product_id'])) {
          $product->save();
        } else {
          $productInfo = $product->where('product_id', $request['product_id'])->update(
            [
              'product_name' => $product->product_name,
              'product_code' => $product->product_code,
              'unit_price' => $product->unit_price,
              'expire_date' => $expireDate,
              'manufactured_date' => $manufactureDate,
              'product_image' => (!empty($request->hasFile('product_image'))) ? $product->product_image : $productInfo->product_image,
              'updated_by' => $userId
            ]);
        }

     return redirect('/products/list');
 }

 public function edit($id){

   $product = new Product();
   $productInfo = $product->where('product_id', $id)->first();

   return view('admin.products.create', compact('productInfo'));
 }
}
