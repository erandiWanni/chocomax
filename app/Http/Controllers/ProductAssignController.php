<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use App\User;
use App\Model\ProductAssign;
use Auth;

class ProductAssignController extends Controller
{
  public function index()
 {
     $productAssign = new ProductAssign();
     $productAssigns = $productAssign->get();

     return view('admin.product_assign.list', compact('productAssigns'));
 }

  public function create()
  {
      $product = new Product();
      $products = $product->get();

      $user = new User();
      $users = $user->where('role_id', 2)->get();

      return view('admin.product_assign.create', compact('products', 'users'));
  }

  public function store(Request $request)
   {
          $userId = Auth::id();

          $productAssign = new ProductAssign();
          $productAssign->product_id = $request['product_id'];
          $productAssign->assign_to = $request['assign_to'];
          $productAssign->quantity = $request['quantity'];
          $productAssign->comments = $request['comments'];
          $productAssign->created_by = $userId;
          $productAssign->updated_by = $userId;


          if(empty($request['product_assign_id'])) {
            $productAssign->save();
          } else {
            $productAssignInfo = $productAssign->where('product_assign_id', $request['product_assign_id'])->update(
              [
                'product_id' => $productAssign->product_id,
                'assign_to' => $productAssign->assign_to,
                'quantity' => $productAssign->quantity,
                'comments' => $productAssign->comments,
                'updated_by' => $userId
              ]);
          }

       return redirect('/product_assign/list');
   }

   public function edit($id){

     $product = new Product();
     $products = $product->get();

     $user = new User();
     $users = $user->where('role_id', 2)->get();

     $productAssign = new ProductAssign();
     $productAssignInfo = $productAssign->where('product_assign_id', $id)->first();

     return view('admin.product_assign.create', compact('productAssignInfo', 'products', 'users'));
   }
}
