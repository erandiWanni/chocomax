<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\RetailShop;
use App\Model\City;
use App\Model\DistributedProducts;
use App\Model\Product;
use App\Model\ProductAssign;
use Auth;

class ReportController extends Controller
{
  public function areaWiseReport()
   {
     $city = new City();
     $cities = $city->get();

     return view('reports.area_wise', compact('cities'));
   }

   public function areaWiseSearch(Request $request)
   {
     $city = new City();
     $cities = $city->get();

     $cityId = $request['city'];

     $distributedProduct = new DistributedProducts();
     $areaWiseDetails = $distributedProduct
                        ->leftJoin('products', 'distributed_product.product_id', 'products.product_id')
                        ->leftJoin('retail_shop', 'distributed_product.shop_id', 'retail_shop.retail_id')
                        ->leftJoin('city', 'retail_shop.city', 'city.id')
                        ->where('retail_shop.city', $cityId)
                        ->select(
                            'products.product_name',
                            'products.product_code',
                            'products.unit_price',
                            'retail_shop.shop_name',
                            'city.name',
                            'distributed_product.quantity'
                        )
                        ->get();

      return view('reports.area_wise', compact('cities', 'areaWiseDetails', 'cityId'));
   }

   public function retailStockReport() {

     $distributedProduct = new DistributedProducts();
     $distributorRetailStocks = $distributedProduct
                        ->leftJoin('products', 'distributed_product.product_id', 'products.product_id')
                        ->leftJoin('retail_shop', 'distributed_product.shop_id', 'retail_shop.retail_id')
                        ->leftJoin('users', 'distributed_product.created_by', 'users.id')
                        ->where('users.role_id', 2)
                        ->select(
                            'products.product_name',
                            'products.product_code',
                            'retail_shop.shop_name',
                            'users.id',
                            'users.name',
                            'distributed_product.quantity'
                        )
                        ->get();

     return view('reports.retail_stock_report', compact('distributorRetailStocks'));
   }

   public function ditributorStockReport() {

     $productAssign = new ProductAssign();
     $distributorStocks = $productAssign
                            ->leftJoin('products', 'product_assign.product_id', 'products.product_id')
                            ->leftJoin('users', 'product_assign.assign_to', 'users.id')
                            ->where('users.role_id', 2)
                            ->select(
                                'products.product_name',
                                'products.product_code',
                                'users.id',
                                'users.name',
                                'product_assign.quantity'
                            )
                            ->get();


     return view('reports.distributor_stock_report', compact('distributorStocks'));
   }

   public function salesReport() {

     $distributedProduct = new DistributedProducts();
     $salesDetails = $distributedProduct
                        ->leftJoin('products', 'distributed_product.product_id', 'products.product_id')
                        ->leftJoin('retail_shop', 'distributed_product.shop_id', 'retail_shop.retail_id')
                        ->leftJoin('users', 'distributed_product.created_by', 'users.id')
                        ->where('users.role_id', 2)
                        ->select(
                            'products.product_name',
                            'products.product_code',
                            'products.unit_price',
                            'retail_shop.shop_name',
                            'users.id',
                            'users.name',
                            'distributed_product.quantity'
                        )
                        ->get();

     return view('reports.sales_report', compact('salesDetails'));
   }
}
