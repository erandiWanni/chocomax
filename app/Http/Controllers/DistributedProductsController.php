<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\RetailShop;
use App\Model\DistributedProducts;
use Auth;

class DistributedProductsController extends Controller
{
  public function index()
 {
    $userId = Auth::id();

     $distributedProduct = new DistributedProducts();

     $roleId = getUserInfo();

     if ($roleId == 1) {
       $distributedProducts = $distributedProduct->get();
     } else {
       $distributedProducts = $distributedProduct->where('created_by', $userId)->get();
     }

     return view('distributors.distributed_products.list', compact('distributedProducts'));
  }

  public function create()
  {
      $userId = Auth::id();

      $product = new Product();
      $products = $product->get();

      $retailShop = new RetailShop();

      $roleId = getUserInfo();

      if ($roleId == 1) {
        $retailShops = $retailShop->get();
      } else {
        $retailShops = $retailShop->where('created_by', $userId)->get();
      }

      return view('distributors.distributed_products.create', compact('products', 'retailShops'));
  }

  public function store(Request $request)
   {
          $userId = Auth::id();

          $distributedProduct = new DistributedProducts();
          $distributedProduct->shop_id = $request['shop_id'];
          $distributedProduct->product_id = $request['product_id'];
          $distributedProduct->quantity = $request['quantity'];
          $distributedProduct->comments = $request['comments'];
          $distributedProduct->created_by = $userId;
          $distributedProduct->updated_by = $userId;

          if(empty($request['distributed_prod_id'])) {
            $distributedProduct->save();
          } else {
            $distributedProductInfo = $distributedProduct->where('distributed_prod_id', $request['distributed_prod_id'])->update(
              [
                'shop_id' => $distributedProduct->shop_id,
                'product_id' => $distributedProduct->product_id,
                'quantity' => $distributedProduct->quantity,
                'comments' => $distributedProduct->comments,
                'updated_by' => $userId
              ]);
          }

       return redirect('/distributed_products/list');
   }

   public function edit($id){

     $userId = Auth::id();
     
     $distributedProduct = new DistributedProducts();
     $distributedProductInfo = $distributedProduct->where('distributed_prod_id', $id)->first();

     $product = new Product();
     $products = $product->get();

     $retailShop = new RetailShop();

     $roleId = getUserInfo();

     if ($roleId == 1) {
       $retailShops = $retailShop->get();
     } else {
       $retailShops = $retailShop->where('created_by', $userId)->get();
     }

     return view('distributors.distributed_products.create', compact('products', 'retailShops', 'distributedProductInfo'));
   }
}
