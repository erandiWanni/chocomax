<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\RetailShop;
use App\Model\City;
use Auth;

class RetailShopController extends Controller
{
  public function index()
 {
    $userId = Auth::id();

     $retailShop = new RetailShop();

     $roleId = getUserInfo();

     if ($roleId == 1) {
       $retailShops = $retailShop->get();
     } else {
       $retailShops = $retailShop->where('created_by', $userId)->get();
     }

     return view('distributors.retail_shops.list', compact('retailShops'));
 }

 public function create()
  {
      $city = new City();
      $cities = $city->get();

      return view('distributors.retail_shops.create', compact('cities'));
  }

  public function store(Request $request)
 {
        $userId = Auth::id();

        $retailShop = new RetailShop();
        $retailShop->shop_name = $request['shop_name'];
        $retailShop->owner_name = $request['owner_name'];
        $retailShop->shop_email = $request['shop_email'];
        $retailShop->contact_number = $request['contact_number'];
        $retailShop->address_1 = $request['address_1'];
        $retailShop->address_2 = $request['address_2'];
        $retailShop->city = $request['city'];
        $retailShop->created_by = $userId;
        $retailShop->updated_by = $userId;

        if(empty($request['retail_id'])) {
          $retailShop->save();
        } else {
          $retailShopInfo = $retailShop->where('retail_id', $request['retail_id'])->update(
            [
              'shop_name' => $retailShop->shop_name,
              'owner_name' => $retailShop->owner_name,
              'shop_email' => $retailShop->shop_email,
              'contact_number' => $retailShop->contact_number,
              'address_1' => $retailShop->address_1,
              'address_2' => $retailShop->address_2,
              'city' => $retailShop->city,
              'updated_by' => $userId
            ]);
        }

     return redirect('/retail_shops/list');
 }

 public function edit($id){

   $retailShop = new RetailShop();
   $retailShopInfo = $retailShop->where('retail_id', $id)->first();

   $city = new City();
   $cities = $city->get();

   return view('distributors.retail_shops.create', compact('retailShopInfo', 'cities'));
 }
}
