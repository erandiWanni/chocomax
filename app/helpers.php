<?php
use App\Model\Product;
use App\User;
use App\Model\RetailShop;
use App\Model\City;

 function getProductNameByID($productId){
  $product = new Product();
  $productInfo = $product->where('product_id', $productId)->first();

  return $productInfo->product_name;
}


function getUserNameByID($userId){
 $user = new User();
 $userInfo = $user->where('id', $userId)->first();

 return $userInfo->name;
}


function getRetailShopNameByID($retailShopId){
 $retailShop = new RetailShop();
 $retailShopInfo = $retailShop->where('retail_id', $retailShopId)->first();

 return $retailShopInfo->shop_name;
}

function getCityNameByID($cityId){
 $city = new City();
 $cityInfo = $city->where('id', $cityId)->first();

 return $cityInfo->name;
}

function getUserInfo() {
    $userId = Auth::id();

    $user = new User();
    $userInfo = $user->where('id', $userId)->first();

    return $userInfo->role_id;
}

 ?>
