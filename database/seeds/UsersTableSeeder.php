<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'Admin',
          'email' => 'admin@gmail.com',
          'password' => bcrypt('123456'),
          'role_id' => '1',
          'created_by' => '1',
          'updated_by' => '1'
      ]);
    }
}
