<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('city')->insert([
        [
          'name' => 'Akkaraipattu',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],
      [
          'name' => 'Ambalangoda',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Ampara',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Anuradhapura',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Badulla',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Balangoda',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Bandarawela',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Batticaloa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Beruwala',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Boralesgamuwa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Chavakachcheri',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Chilaw',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Colombo',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Dambulla',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Dehiwala-Mount Lavinia',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Embilipitiya',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Eravur',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Galle',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Gampaha',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Gampola',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Hambantota',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Haputale',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Hatton-Dickoya',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Hikkaduwa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Horana',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Ja-Ela',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Jaffna',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kadugannawa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kaduwela (Battaramulla)',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kalmunai (incl. Sainthamarathu)',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kalutara',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kandy',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kattankudy (Kathankudi)',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Katunayake (-Seeduwa)',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kegalle',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kesbewa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kilinochchi',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kinniya',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kolonnawa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kuliyapitiya ',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Kurunegala',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Maharagama',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Mannar',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Matale',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Matara',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Minuwangoda',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Moneragala',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Moratuwa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Mullaitivu',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Nawalapitiya',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Negombo',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Nuwara Eliya',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Panadura',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Peliyagoda',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Point Pedro',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Polonnaruwa',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Puttalam',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Ratnapura',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Seethawakapura (Avissawella)',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Sri Jayawardenepura (Kotte)',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Tangalle',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Thalawakele-Lindula',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Trincomalee',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Valvettithurai',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Vavuniya',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Wattala-Mabole',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Wattegama',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ],[
          'name' => 'Weligama',
          'description' => '',
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'deleted_at' => null,
      ]
      ]);
    }
}
