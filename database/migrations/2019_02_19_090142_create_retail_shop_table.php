<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetailShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retail_shop', function (Blueprint $table) {
            $table->increments('retail_id');
            $table->string('shop_name');
            $table->string('owner_name');
            $table->string('shop_email')->unique();
            $table->string('contact_number');
            $table->string('address_1');
            $table->string('address_2');
            $table->unsignedInteger('city');
            $table->foreign('city')->references('id')->on('city');
            $table->unsignedInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedInteger('updated_by');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retail_shops');
    }
}
