@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-8">
              <div id="heading">
                <h1>{{(!empty($retailShopInfo->retail_id)) ? "Edit" : "Add"}} Retail Shop Form</h1>
              </div>

              <form method="POST" action="/retail_shops/store">
                @csrf

                <input type="hidden" id="retail_id" name="retail_id" value="{{(!empty($retailShopInfo->retail_id)) ? $retailShopInfo->retail_id : ""}}"/>

                <div class="form-group row">
                    <label for="shop_name" class="col-md-4 col-form-label text-md-right">Shop Name <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="shop_name" type="text" name="shop_name"  class="form-control" required value="{{(!empty($retailShopInfo->shop_name)) ? $retailShopInfo->shop_name : ""}}">

                        @if ($errors->has('shop_name'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('shop_name') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="owner_name" class="col-md-4 col-form-label text-md-right">Owner Name <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="owner_name" type="text" name="owner_name"  class="form-control" required value="{{(!empty($retailShopInfo->owner_name)) ? $retailShopInfo->owner_name : ""}}">

                        @if ($errors->has('owner_name'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('owner_name') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="shop_email" class="col-md-4 col-form-label text-md-right">Shop Email <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="shop_email" type="email" name="shop_email"  class="form-control" required value="{{(!empty($retailShopInfo->shop_email)) ? $retailShopInfo->shop_email : ""}}">

                        @if ($errors->has('shop_email'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('shop_email') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact_number" class="col-md-4 col-form-label text-md-right">Contact Number <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="contact_number" type="tel" name="contact_number"  class="form-control" required value="{{(!empty($retailShopInfo->contact_number)) ? $retailShopInfo->contact_number : ""}}">

                        @if ($errors->has('contact_number'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('contact_number') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address_1" class="col-md-4 col-form-label text-md-right">Address 1 <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="address_1" type="text" name="address_1"  class="form-control" required value="{{(!empty($retailShopInfo->address_1)) ? $retailShopInfo->address_1 : ""}}">

                        @if ($errors->has('address_1'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('address_1') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address_2" class="col-md-4 col-form-label text-md-right">Address 2 <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="address_2" type="text" name="address_2"  class="form-control" required value="{{(!empty($retailShopInfo->address_2)) ? $retailShopInfo->address_2 : ""}}">

                        @if ($errors->has('address_2'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('address_2') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="city" class="col-md-4 col-form-label text-md-right">City <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <select id="city" name="city" class="form-control" required>

                          @if(!empty($retailShopInfo->city))
                            <option value="{{$retailShopInfo->city}}" selected>{{getCityNameByID($retailShopInfo->city)}}</option>
                          @else
                            <option value="" selected disabled>Select an option</option>
                          @endif

                          @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->name}}</option>
                          @endforeach
                        </select>

                        @if ($errors->has('city'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('city') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">{{(!empty($retailShopInfo->retail_id)) ? "Update" : "Save"}}</button>
                    </div>
                </div>
              </form>
        </div>
    </div>
</div>
@endsection
