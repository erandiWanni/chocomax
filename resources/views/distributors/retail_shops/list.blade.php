@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div id="createButton">
          <a href="/retail_shops/create" class="btn btn-info" role="button">Add Retail Shop</a>
        </div>

        <br>

        <div class="col-md-12">
          <div id="heading">
            <h1>Retails Shops</h1>
          </div>
          <div class="table-responsive">
              <table id="retailShopsTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Owner Name</th>
                      <th>Email</th>
                      <th>Number</th>
                      <th>City</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($retailShops as $retailShop)
                  <tr>
                      <td>{{$retailShop->retail_id}}</td>
                      <td>{{$retailShop->shop_name}}</td>
                      <td>{{$retailShop->owner_name}}</td>
                      <td>{{$retailShop->shop_email}}</td>
                      <td>{{$retailShop->contact_number}}</td>
                      <td>{{getCityNameByID($retailShop->city)}}</td>
                      <td><a href="/retail_shops/edit/{{$retailShop->retail_id}}"><i class="glyphicon glyphicon-edit"></i></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#retailShopsTable').DataTable({

  });
});
</script>
@endsection
