@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-8">
              <div id="heading">
                <h1>{{(!empty($distributedProductInfo->distributed_prod_id)) ? "Edit" : "Add"}} Product Distributed Form</h1>
              </div>

              <form method="POST" action="/distributed_products/store">
                @csrf
                <input type="hidden" id="distributed_prod_id" name="distributed_prod_id" value="{{(!empty($distributedProductInfo->distributed_prod_id)) ? $distributedProductInfo->distributed_prod_id : ""}}"/>

                <div class="form-group row">
                    <label for="shop_id" class="col-md-4 col-form-label text-md-right">Select Shop <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <select id="shop_id" name="shop_id"  class="form-control" required>

                          @if(!empty($distributedProductInfo->shop_id))
                            <option value="{{$distributedProductInfo->shop_id}}" selected>{{getRetailShopNameByID($distributedProductInfo->shop_id)}}</option>
                          @else
                            <option value="" selected disabled>Select an option</option>
                          @endif

                          @foreach($retailShops as $retailShop)
                            <option value="{{$retailShop->retail_id}}">{{$retailShop->shop_name}}</option>
                          @endforeach
                        </select>

                        @if ($errors->has('shop_id'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('shop_id') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="product_id" class="col-md-4 col-form-label text-md-right">Select Product <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <select id="product_id" name="product_id"  class="form-control" required>
                          @if(!empty($distributedProductInfo->product_id))
                            <option value="{{$distributedProductInfo->product_id}}" selected>{{getProductNameByID($distributedProductInfo->product_id)}}</option>
                          @else
                            <option value="" selected disabled>Select an option</option>
                          @endif

                          @foreach($products as $product)
                            <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                          @endforeach
                        </select>

                        @if ($errors->has('product_id'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_id') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="quantity" class="col-md-4 col-form-label text-md-right">Quantity <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="quantity" type="number" name="quantity"  class="form-control" required value="{{(!empty($distributedProductInfo->quantity)) ? $distributedProductInfo->quantity : ""}}"/>

                        @if ($errors->has('quantity'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('quantity') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="comments" class="col-md-4 col-form-label text-md-right">Comments <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <textarea id="comments" type="text" name="comments"  class="form-control" required>{{(!empty($distributedProductInfo->comments)) ? $distributedProductInfo->comments : ""}}</textarea>

                        @if ($errors->has('comments'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('comments') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">{{(!empty($distributedProductInfo->distributed_prod_id)) ? "Update" : "Save"}}</button>
                    </div>
                </div>
              </form>
        </div>
    </div>
</div>
@endsection
