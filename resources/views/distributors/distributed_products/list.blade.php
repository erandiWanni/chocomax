@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div id="createButton">
          <a href="/distributed_products/create" class="btn btn-info" role="button">Add Distributed Products</a>
        </div>

        <br>

        <div class="col-md-12">
          <div id="heading">
            <h1>Distributed Products</h1>
          </div>
          <div class="table-responsive">
              <table id="distributedProductsTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Shop</th>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>Comments</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($distributedProducts as $distributedProduct)
                  <tr>
                      <td>{{ $distributedProduct->distributed_prod_id }}</td>
                      <td>{{getRetailShopNameByID($distributedProduct->shop_id)}}</td>
                      <td>{{getProductNameByID($distributedProduct->product_id)}}</td>
                      <td>{{$distributedProduct->quantity}}</td>
                      <td>{{$distributedProduct->comments}}</td>
                      <td><a href="/distributed_products/edit/{{$distributedProduct->distributed_prod_id}}"><i class="glyphicon glyphicon-edit"></i></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#distributedProductsTable').DataTable({

  });
});
</script>
@endsection
