@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div id="createButton">
          <a href="/requested_products/create" class="btn btn-info" role="button">Add Requested Products</a>
        </div>

        <br>

        <div class="col-md-12">
          <div id="heading">
            <h1>Requested Products</h1>
          </div>
          <div class="table-responsive">
              <table id="requestedProductsTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Shop</th>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>Comments</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($requestedProducts as $requestedProduct)
                  <tr>
                      <td>{{$requestedProduct->request_prod_id }}</td>
                      <td>{{getRetailShopNameByID($requestedProduct->shop_id)}}</td>
                      <td>{{getProductNameByID($requestedProduct->product_id)}}</td>
                      <td>{{$requestedProduct->quantity}}</td>
                      <td>{{$requestedProduct->feedback}}</td>
                      <td><a href="/requested_products/edit/{{$requestedProduct->request_prod_id}}"><i class="glyphicon glyphicon-edit"></i></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#requestedProductsTable').DataTable({

  });
});
</script>
@endsection
