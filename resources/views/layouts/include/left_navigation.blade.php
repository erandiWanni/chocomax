<div class="wrapper">

    <!-- Sidebar -->
    <nav id="sidebar">

      <ul class="list-unstyled">
            @if(getUserInfo() == 1)
            <li class="active">
                <a href="#adminSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Admin Menu</a>
                <ul class="collapse list-unstyled" id="adminSubmenu">
                    <li>
                        <a href="/products/list">Products</a>
                    </li>
                    <li>
                        <a href="/product_assign/list">Product Assign</a>
                    </li>
                </ul>
            </li>
            @endif

            <li>
                <a href="#distributerSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Distributer Menu</a>
                <ul class="collapse list-unstyled" id="distributerSubmenu">
                    <li>
                        <a href="/retail_shops/list">Retail Shops</a>
                    </li>
                    <li>
                        <a href="/distributed_products/list">Distributed Products</a>
                    </li>
                    <li>
                        <a href="/requested_products/list">Requested Products</a>
                    </li>
                </ul>
            </li>

            @if(getUserInfo() == 1)
            <li>
                <a href="#reportsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Reports</a>
                <ul class="collapse list-unstyled" id="reportsSubmenu">
                    <li>
                        <a href="/reports/sales">Sales Reports</a>
                    </li>
                    <li>
                        <a href="/reports/area_wise">Area Wise Weekly Sales Reports</a>
                    </li>
                    <li>
                        <a href="/reports/retails_stock_report">Retail Shop Stock Reports</a>
                    </li>
                    <li>
                        <a href="/reports/distributor_stock_report">Distributor Stock Reports</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </nav>

    <!-- Page Content -->
    <div id="content">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <!-- <button type="button" id="sidebarCollapse" class="btn btn-info">
                <i class="fas fa-align-left"></i>
                <span>Toggle Sidebar</span>
            </button> -->

            @yield('content')


        </div>
    </nav>
    </div>
</div>
