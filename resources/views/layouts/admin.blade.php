<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.include.header')
    @yield('style')


    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.include.left_navigation')

    </div>
    @include('layouts.include.footer')
    @yield('script')

</body>
</html>
