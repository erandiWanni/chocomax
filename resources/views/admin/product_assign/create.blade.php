@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-8">
              <div id="heading">
                <h1>{{(!empty($productAssignInfo->product_assign_id)) ? "Edit" : "Add"}} Product Assign Form</h1>
              </div>

              <form method="POST" action="/product_assign/store">
                @csrf
                <input type="hidden" id="product_assign_id" name="product_assign_id" value="{{(!empty($productAssignInfo->product_assign_id)) ? $productAssignInfo->product_assign_id : ""}}"/>

                <div class="form-group row">
                    <label for="product_id" class="col-md-4 col-form-label text-md-right">Select Product <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <select id="product_id" name="product_id"  class="form-control" required>

                          @if(!empty($productAssignInfo->product_id))
                            <option value="{{$productAssignInfo->product_id}}">{{getProductNameByID($productAssignInfo->product_id)}}</option>
                          @else
                            <option value="" selected disabled>Select an option</option>
                          @endif

                          @foreach($products as $product)
                            <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                          @endforeach
                        </select>

                        @if ($errors->has('product_id'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_id') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="assign_to" class="col-md-4 col-form-label text-md-right">Assign To <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <select id="assign_to" name="assign_to"  class="form-control" required>

                          @if(!empty($productAssignInfo->assign_to))
                            <option value="{{$productAssignInfo->assign_to}}">{{getUserNameByID($productAssignInfo->assign_to)}}</option>
                          @else
                            <option value="" selected disabled>Select an option</option>
                          @endif

                          @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                          @endforeach
                        </select>

                        @if ($errors->has('assign_to'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('assign_to') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="quantity" class="col-md-4 col-form-label text-md-right">Quantity <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="quantity" type="number" name="quantity"  class="form-control" required value="{{(!empty($productAssignInfo->quantity)) ? $productAssignInfo->quantity : ""}}"/>

                        @if ($errors->has('quantity'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('quantity') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="comments" class="col-md-4 col-form-label text-md-right">Comments <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <textarea id="comments" type="text" name="comments"  class="form-control" required>{{(!empty($productAssignInfo->comments)) ? $productAssignInfo->comments : ""}}</textarea>

                        @if ($errors->has('comments'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('comments') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">{{(!empty($productAssignInfo->product_id)) ? "Update" : "Save"}}</button>
                    </div>
                </div>
              </form>
        </div>
    </div>
</div>
@endsection
