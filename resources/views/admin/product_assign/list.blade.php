@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div id="createButton">
          <a href="/product_assign/create" class="btn btn-info" role="button">Add Product Assign</a>
        </div>

        <br>

        <div class="col-md-12">
          <div id="heading">
            <h1>Product Assign</h1>
          </div>
          <div class="table-responsive">
              <table id="productAssignTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Assign To</th>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>Comments</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($productAssigns as $productAssign)
                  <tr>
                      <td>{{ $productAssign->product_assign_id }}</td>
                      <td>{{getUserNameByID($productAssign->assign_to)}}</td>
                      <td>{{getProductNameByID($productAssign->product_id)}}</td>
                      <td>{{$productAssign->quantity}}</td>
                      <td>{{$productAssign->comments}}</td>
                      <td><a href="/product_assign/edit/{{$productAssign->product_assign_id}}"><i class="glyphicon glyphicon-edit"></i></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#productAssignTable').DataTable({

  });
});
</script>
@endsection
