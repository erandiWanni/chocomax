@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div id="createButton">
          <a href="/products/create" class="btn btn-info" role="button">Add Product</a>
        </div>

        <br>


        <div class="col-md-12">
          <div id="heading">
            <h1>Products</h1>
          </div>
          <div class="table-responsive">
              <table id="productsTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Code</th>
                      <th>Unit Price</th>
                      <th>Expire date</th>
                      <th>Manufacture Date</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($products as $product)
                  <tr>
                      <td>{{ $product->product_id }}</td>
                      <td>{{$product->product_name}}</td>
                      <td>{{$product->product_code}}</td>
                      <td>{{$product->unit_price}}</td>
                      <td>{{$product->expire_date}}</td>
                      <td>{{$product->manufactured_date}}</td>
                      <td>
                        <a href="/products/edit/{{$product->product_id}}"><i class="glyphicon glyphicon-edit"></i></a>

                        @if(!empty($product->product_image))
                          <a href="/images/{{$product->product_image}}" download><i class="glyphicon glyphicon-download-alt"></i></a>
                        @endif
                      </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#productsTable').DataTable({

  });
});
</script>
@endsection
