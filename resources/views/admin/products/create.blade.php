@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-8">
              <div id="heading">
                <h1>{{(!empty($productInfo->product_id)) ? "Edit" : "Add"}} Product Form</h1>
              </div>

              <form method="POST" action="/products/store" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="product_id" name="product_id" value="{{(!empty($productInfo->product_id)) ? $productInfo->product_id : ""}}"/>

                <div class="form-group row">
                    <label for="product_name" class="col-md-4 col-form-label text-md-right">Name <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="product_name" type="text" name="product_name"  class="form-control" required value="{{(!empty($productInfo->product_name)) ? $productInfo->product_name : ""}}">

                        @if ($errors->has('product_name'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_name') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="product_code" class="col-md-4 col-form-label text-md-right">Code <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="product_code" type="text" name="product_code"  class="form-control" required value="{{(!empty($productInfo->product_code)) ? $productInfo->product_code : ""}}">

                        @if ($errors->has('product_code'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_code') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="product_unit_price" class="col-md-4 col-form-label text-md-right">Unit Price <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <input id="product_unit_price" type="text" name="product_unit_price"  class="form-control" required value="{{(!empty($productInfo->unit_price)) ? $productInfo->unit_price : ""}}">

                        @if ($errors->has('product_unit_price'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_unit_price') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="product_expire_date" class="col-md-4 col-form-label text-md-right">Expire Date <font color="red">*</font></label>

                    <div class="col-md-6 input-group date">
                      <input type='text' class="form-control date" id="product_expire_date" name="product_expire_date" required value="{{(!empty($productInfo->expire_date)) ? date('d-m-Y', strtotime(str_replace('-', '/', $productInfo->expire_date))) : ""}}"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>

                        @if ($errors->has('product_expire_date'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_expire_date') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="product_manufacture_date" class="col-md-4 col-form-label text-md-right">Manufacture Date <font color="red">*</font></label>

                    <div class="col-md-6 input-group date">
                      <input type='text' class="form-control date" id="product_manufacture_date" name="product_manufacture_date" required value="{{(!empty($productInfo->manufactured_date)) ? date('d-m-Y', strtotime(str_replace('-', '/', $productInfo->manufactured_date))) : ""}}"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>

                        @if ($errors->has('product_manufacture_date'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_manufacture_date') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="product_image" class="col-md-4 col-form-label text-md-right">Image <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                      <input type='file' class="form-control" id="product_image" name="product_image"/>

                        @if ($errors->has('product_image'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('product_image') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">{{(!empty($productInfo->product_id)) ? "Update" : "Save"}}</button>
                    </div>
                </div>
              </form>
        </div>
    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">

    $('.date').datepicker({

       format: 'dd-mm-yyyy'

     });

</script>
@endsection
