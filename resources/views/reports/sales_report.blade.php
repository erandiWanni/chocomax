@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div id="heading">
            <h1>Sales Report</h1>
          </div>
          <div class="table-responsive">
              <table id="salesReportTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Retail Shop Name</th>
                      <th>Distributor Name</th>
                      <th>Product Code</th>
                      <th>Product Name</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Line Total</th>
                  </tr>
              </thead>
              <tbody>
                <?php $grandTotal = 0; ?>
                @foreach($salesDetails as $salesDetail)

                <?php
                $lineTotal = $salesDetail->unit_price * $salesDetail->quantity;

                $grandTotal =  $grandTotal + $lineTotal;
                ?>

                  <tr>
                      <td>{{$salesDetail->shop_name}}</td>
                      <td>{{$salesDetail->name}}</td>
                      <td>{{$salesDetail->product_code}}</td>
                      <td>{{$salesDetail->product_name}}</td>
                      <td align="right">{{$salesDetail->quantity}}</td>
                      <td align="right">{{number_format((float)$salesDetail->unit_price, 2, '.', '')}}</td>
                      <td align="right">{{number_format((float)$lineTotal, 2, '.', '')}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <br><br>
          <h4 align="right"><b>Grand Total : </b></h4>
          <h1 align="right">Rs. {{number_format((float)$grandTotal, 2, '.', '')}}</h1>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#salesReportTable').DataTable({

  });
});
</script>
@endsection
