@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-8">
              <div id="heading">
                <h1>Area Wise Report</h1>
              </div>

              <form method="POST" action="/reports/area_wise/search">
                @csrf
          <div class="col-md-12">
            <div class="col-md-6">
                <div class="form-group row">
                    <label for="city" class="col-md-6 col-form-label text-md-right">Select a City <font color="red">*</font></label>

                    <div class="col-md-6 input-group">
                        <select id="city" name="city" class="form-control" required>


                          @if(!empty($cityId))
                            <option value="{{$cityId}}" selected>{{getCityNameByID($cityId)}}</option>\
                          @else
                            <option value="" selected disabled>Select an option</option>
                          @endif

                          @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->name}}</option>
                          @endforeach
                        </select>

                        @if ($errors->has('city'))
                          <span class="form-control" role="alert">
                            <strong>{{ $errors->first('city') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-md-6 ">
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
              </div>
              </form>
            </div>
        </div>
    </div>

    <br><br><br>

    <div class="col-md-9">
      @if(!empty($areaWiseDetails))
      <div class="table-responsive">
          <table id="areaWiseTable" class="table table-striped table-bordered" style="width:100%">
          <thead>
              <tr>
                  <th>Shop Name</th>
                  <th>Product Code</th>
                  <th>Product Name</th>
                  <th>Unit Price</th>
                  <th>Quantity</th>
                  <th>Line Total</th>
              </tr>
          </thead>
          <tbody>
            <?php $grandTotal = 0; ?>
            @foreach($areaWiseDetails as $areaWiseDetail)
              <?php
              $lineTotal = $areaWiseDetail->unit_price * $areaWiseDetail->quantity;

              $grandTotal =  $grandTotal + $lineTotal;
              ?>
              <tr>
                  <td>{{$areaWiseDetail->shop_name}}</td>
                  <td>{{$areaWiseDetail->product_code}}</td>
                  <td>{{$areaWiseDetail->product_name}}</td>
                  <td align="right">{{$areaWiseDetail->unit_price}}</td>
                  <td align="right">{{$areaWiseDetail->quantity}}</td>
                  <td align="right">{{number_format((float)$lineTotal, 2, '.', '')}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <br><br>
      <h4 align="right"><b>Grand Total : </b></h4>
      <h1 align="right">Rs. {{number_format((float)$grandTotal, 2, '.', '')}}</h1>
    @endif
    </div>
</div>
</div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#areaWiseTable').DataTable({

  });
});
</script>
@endsection
