@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div id="heading">
            <h1>Retail Shop Stock Report</h1>
          </div>
          <div class="table-responsive">
              <table id="retailStockReportTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Retail Shop Name</th>
                      <th>Distributor Name</th>
                      <th>Product Code</th>
                      <th>Product Name</th>
                      <th>Quantity</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($distributorRetailStocks as $distributorRetailStock)
                  <tr>
                      <td>{{$distributorRetailStock->shop_name}}</td>
                      <td>{{$distributorRetailStock->name}}</td>
                      <td>{{$distributorRetailStock->product_code}}</td>
                      <td>{{$distributorRetailStock->product_name}}</td>
                      <td>{{$distributorRetailStock->quantity}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#retailStockReportTable').DataTable({

  });
});
</script>
@endsection
