@extends('layouts.admin')

@extends('layouts.include.top_menu')

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div id="heading">
            <h1>Distributor Stock Report</h1>
          </div>
          <div class="table-responsive">
              <table id="distributorReportTable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Distributor ID</th>
                      <th>Distributor Name</th>
                      <th>Product Code</th>
                      <th>Product Name</th>
                      <th>Quantity</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($distributorStocks as $distributorStock)
                  <tr>
                      <td>{{$distributorStock->id}}</td>
                      <td>{{$distributorStock->name}}</td>
                      <td>{{$distributorStock->product_code}}</td>
                      <td>{{$distributorStock->product_name}}</td>
                      <td align="right">{{$distributorStock->quantity}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('script')

<script>
$(document).ready(function() {
  $('#distributorReportTable').DataTable({

  });
});
</script>
@endsection
