<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Products
Route::get('products/list', 'ProductController@index');
Route::get('products/create', 'ProductController@create');
Route::post('products/store', 'ProductController@store');
Route::get('products/edit/{id}', 'ProductController@edit');

Route::get ('retail_shops/list', 'RetailShopController@index');
Route::get('retail_shops/create', 'RetailShopController@create');
Route::post('retail_shops/store', 'RetailShopController@store');
Route::get('retail_shops/edit/{id}', 'RetailShopController@edit');

Route::get('product_assign/list', 'ProductAssignController@index');
Route::get('product_assign/create', 'ProductAssignController@create');
Route::post('product_assign/store', 'ProductAssignController@store');
Route::get('product_assign/edit/{id}', 'ProductAssignController@edit');

Route::get ('distributed_products/list', 'DistributedProductsController@index');
Route::get('distributed_products/create', 'DistributedProductsController@create');
Route::post('distributed_products/store', 'DistributedProductsController@store');
Route::get('distributed_products/edit/{id}', 'DistributedProductsController@edit');

Route::get ('requested_products/list', 'RequestedProductsController@index');
Route::get('requested_products/create', 'RequestedProductsController@create');
Route::post('requested_products/store', 'RequestedProductsController@store');
Route::get('requested_products/edit/{id}', 'RequestedProductsController@edit');

Route::get('reports/area_wise', 'ReportController@areaWiseReport');
Route::post('reports/area_wise/search', 'ReportController@areaWiseSearch');

Route::get('reports/retails_stock_report', 'ReportController@retailStockReport');

Route::get('reports/distributor_stock_report', 'ReportController@ditributorStockReport');

Route::get('reports/sales', 'ReportController@salesReport');
